package com.jc.android.rxjava.exemplesimplelivedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {

    private MutableLiveData<Integer> mutableLiveDataInteger;
    private MutableLiveData<String> mutableLiveDataString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLiveData();
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                majData((int) Math.floor(Math.random() * 100));
            }
        });
    }

    private void initLiveData() {
        mutableLiveDataInteger = new MutableLiveData<Integer>();
        mutableLiveDataInteger.setValue(5); // déclenche le onChanged !

        mutableLiveDataString = new MutableLiveData<String>();
        mutableLiveDataString.setValue("Hello "); // déclenche le onChanged !

        mutableLiveDataInteger.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mutableLiveDataString.setValue("nouvelle valeur : " + integer);
                printResultInteger("maj de Integer ! ");
            }
        });

        mutableLiveDataString.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                printResultString("maj de String ! ");
            }
        });
        // printResult("au départ ");
    }

    private void majData(int i) {
        mutableLiveDataInteger.setValue(i);
    }

    private void printResultInteger(String msg) {
        Log.d("JCT", msg + "Integer = " + mutableLiveDataInteger.getValue().toString());
        Log.d("JCT", msg + "String = " + mutableLiveDataString.getValue());
    }

    private void printResultString(String msg) {
        Log.d("JCT", msg + "String = " + mutableLiveDataString.getValue());
    }

}